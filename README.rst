This is a re-implementation of some classic PDP-11 games with the curses API.
Super-Pacman (sp21), Mars (marswar) and Xonix (xonix) have been implemented
so far.

Any contributions to the project are always welcome. Please direct your
questions, comments or patches to Peter Cherepanov <sphinx.pinastri@gmail.com>

The goal of sp21 is to collect the all dots in the maze and avoid ghosts.
The game is controlled with the arrow keys.

The goal of marswar is to shoot down all the martians and defend your soldiers.
The game is controlled with the arrow keys and space bar.

The goal of xonix is to claim as much land as possible while avoiding the
enemies. The game is also controlled with the arrow keys.
